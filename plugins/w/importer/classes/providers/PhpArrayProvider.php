<?php namespace W\Importer\Classes\Providers;

use League\Csv\Reader as CsvReader;
use W\Seeder\Classes\Providers\ProviderBase;

/*
 * This provider works with original php array
 */
class PhpArrayProvider extends ProviderBase
{
    /*
     * Return resolved data
     */
    public function getData()
    {
        return $this->inputData;
    }

    /*
     * Determinate whether provider is workable
     */
    public static function isWorkable($inputData)
    {
        return is_array($inputData);
    }
}
