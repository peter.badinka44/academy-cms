<?php namespace W\Importer\Classes;

use Exception;
use League\Csv\Reader;
use Illuminate\Support\Facades\Event;
use W\Seeder\Classes\Seeder;
use W\Importer\Classes\Parsers\HeaderParser;
use W\Importer\Classes\Parsers\RecordsParser;
use W\Importer\Classes\Parsers\UtilParser;

class Importer
{
    /*
     * Import
     */
    public static function import($data, $additionalAliases = [], $additionalIgnore = [])
    {
        $ignore = UtilParser::mergeIgnoreColumns($additionalIgnore);
        $aliases = UtilParser::mergeAliases($additionalAliases);
        $data = UtilParser::dataFromCsvStrings($data);
        
        Event::fire('w.importer.beforeImport', [&$data, &$aliases, &$ignore]);
        
        $processedRecords = [];
        
        foreach ($data as $sheetKey => $singleData) {
            try {
                $records = self::prepareRecords($singleData, $aliases, $ignore);
                Event::fire('w.importer.beforeSeedData', [&$records]);
                $processedRecords[$sheetKey] = self::seed($records, $aliases);
            } catch (Exception $exception) {
                // Add location where exception happened
                throw new Exception(sprintf('File: %s %s', $sheetKey, $exception->getMessage()));
            }
        }
        
        Event::fire('w.importer.import', [&$processedRecords]);
        
        return $processedRecords;
    }
    
    /*
     * Prepare single file data
     */
    protected static function prepareRecords($data, $aliases, $ignore)
    {
        $reader = Reader::createFromString($data);
        $header = HeaderParser::prepareHeader($reader, $aliases);
        
        return RecordsParser::prepareRecords($reader, $header, $ignore);
    }
    
    /*
     * Seed from prepared data
     */
    protected static function seed($data, $aliases)
    {
        $records = [];
        
        foreach ($data as $modelAlias => $records) {
            $model = $aliases[$modelAlias];
            $records[$model] = Seeder::seed($model, $records);
        }
        
        return $records;
    }
}