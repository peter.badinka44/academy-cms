<?php namespace W\Importer;

use Backend;
use Illuminate\Support\Facades\Event;
use LibFeature\Lesson\Models\Lesson;
use System\Classes\PluginBase;
use W\Importer\Classes\Providers\PhpArrayProvider;
use W\Seeder\Facades\SeederProviders;

/**
 * Importer Plugin Information File
 */
class Plugin extends PluginBase
{
    public $require = 'W.Seeder';

    public function pluginDetails()
    {
        return [
            'name'        => 'Importer',
            'description' => 'Imports data from CSV file',
            'author'      => 'W',
            'icon'        => 'icon-list'
        ];
    }
    
    public function boot()
    {
        SeederProviders::add([
            PhpArrayProvider::class
        ]);
        
        Event::listen('w.seeder.beforeSave', function($model, $data) {
            // if running in console, dont fire importer event
            if (app()->runningInConsole()) {
                return;
            }
            // return importer event
            return Event::fire('w.importer.beforeSave', [$model, $data], true);
        });
    }


    public function registerNavigation()
    {
        return [
            'importer' => [
                'label'       => 'Import',
                'url'         => Backend::url('w/importer/import'),
                'icon'        => 'icon-list',
            ],
        ];
    }
}
