<?php namespace W\Importer\Controllers;

use Backend\Classes\Controller;
use Exception;
use October\Rain\Support\Facades\Flash;
use Illuminate\Support\Facades\Input;
use W\Importer\Classes\Importer;
use W\Importer\Classes\Services\ImportService;

/**
 * Importer Back-end Controller
 */
class Import extends Controller
{
    /*
     * Index action
     */
    public function index()
    {
        $this->addJs('/plugins/w/importer/controllers/import/assets/js/main.js');
        $this->addCss('/plugins/w/importer/controllers/import/assets/css/style.css');
    }
    
    /*
     * Index onImport ajax handler
     */
    public function index_onImport()
    {
        $aliases = ImportService::getAliasesFromRequest();
        
        if (!Input::hasFile('files')) {
            Flash::error('Files are missing from the request');
            return;
        }
        
        $data = ImportService::getDataFromRequests();
        
        try {
            Importer::import($data, $aliases);
            Flash::success('Successfully imported');
            
        } catch (Exception $exception) {
            Flash::error($exception->getMessage());
        }
    }
    
}
