<?php namespace App\Lessons\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateLessonsTable Migration
 */
class CreateLessonsTable extends Migration
{
    public function up()
    {
        Schema::create('app_lessons', function (Blueprint $table) {
            $table->increments('id');            
            $table->string('name')->nullable()->index();
            $table->string('slug')->nullable()->unique();            
            $table->text('tags_string')->nullable();
            $table->text('video_tags_string')->nullable();
            $table->boolean('is_published')->default(false)->index();            
            $table->text('content_description')->nullable();
            $table->text('thumbnail')->nullable();
            $table->text('content_video')->nullable();
            $table->unsignedInteger('course_id')->nullable()->index();
            $table->unsignedInteger('sort_order')->nullable()->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('app_lessons');
    }
}
