<?php namespace App\Lessons;

use Backend;
use System\Classes\PluginBase;

/**
 * lessons Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'lessons',
            'description' => 'No description provided yet...',
            'author'      => 'app',
            'icon'        => 'icon-pencil'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'App\Lessons\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'app.lessons.some_permission' => [
                'tab' => 'lessons',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'lessons' => [
                'label'       => 'lessons',
                'url'         => Backend::url('app/lessons/lessons'),
                'icon'        => 'icon-pencil',
                'permissions' => ['app.lessons.*'],
                'order'       => 3,
            ],
        ];
    }
}
