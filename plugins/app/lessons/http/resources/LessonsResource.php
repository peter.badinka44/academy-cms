<?php 

namespace App\Lessons\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use LibUser\UserApi\Facades\JWTAuth;

class LessonsResource extends Resource
{
	public function toArray($request)
	{
		$likeByActiveUser = null;
		$bookmarkedByActiveUser = null;
		$user = JWTAuth::getUser();

		if ($user) {
			$likeByActiveUser = $this->flags()->where('user_id', $user->id)->where('type', 'like')->sum('value');
			$bookmarkedByActiveUser = $this->flags()->where('user_id', $user->id)->where('type', 'bookmark')->sum('value');
		}
		
		$response = [
			'id'						=> $this->id,
			'name'						=> $this->name,
			'slug'						=> $this->slug,
			'content_description'		=> $this->content_description,
			'content_thumbnail'			=> $this->thumbnail,
			'tags_string'				=> $this->tags_string,
			'tags'						=> $this->tags,
			'created_at'				=> $this->created_at->toDateTimeString(),
			'updated_at'				=> $this->updated_at->toDateTimeString(),
			'likes'						=> $this->flags()->where('type', 'like')->where('value', 1)->sum('value'),
			'liked_by_active_user' 		=> $likeByActiveUser,
			'bookmarked_by_active_user'	=> $bookmarkedByActiveUser
		];

		return $response;
	}
}