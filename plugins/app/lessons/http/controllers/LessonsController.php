<?php 

namespace App\Lessons\Http\Controllers;

use App\Lessons\Models\Lesson;
use App\Lessons\Http\Resources\LessonsResource;
use Illuminate\Routing\Controller;
use LibUser\UserApi\Facades\JWTAuth;

class LessonsController extends Controller
{
	/**
	 * index
	 */
	public function index()
	{
		return LessonsResource::collection(
			Lesson::isPublished()
				->with('flags')
				->paginate(get('result_per_page') ?? 100000)
		);
	}

	/**
	 * show
	 */
	public function show($slug)
	{
		return new LessonsResource(
			Lesson::isPublished()
				->where('slug', $slug)
				->firstOrFail()
		);
	}

}