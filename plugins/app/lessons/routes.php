<?php

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use WApi\ApiException\Http\Middlewares\ApiExceptionMiddleware;

Route::group([
	'prefix' 		=> 'api/v1',
	'namespace' 	=> 'App\Lessons\Http\Controllers',
	'middleware' 	=> [ApiExceptionMiddleware::class, 'api'],
], function(Router $router) {
	$router->get('/lessons', 'LessonsController@index');
	$router->get('/lessons/{slug}', 'LessonsController@show');
});