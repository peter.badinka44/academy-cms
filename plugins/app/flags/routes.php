<?php

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use WApi\ApiException\Http\Middlewares\ApiExceptionMiddleware;

Route::group([
	'prefix' 		=> 'api/v1',
	'namespace' 	=> 'App\Flags\Http\Controllers',
	'middleware' 	=> [ApiExceptionMiddleware::class, 'api'],
], function(Router $router) {
	$router->post('/flags/lesson', 'FlagsLessonController@updateOrCreate');
});