<?php

namespace App\Flags\Http\Controllers;

use Illuminate\Routing\Controller;
use App\Lessons\Models\Lesson;
use LibUser\UserApi\Facades\JWTAuth;

class FlagsLessonController extends Controller
{
	public function updateOrCreate()
	{
		$type = post('flag_type');
		$key = post('key');
		$value = post('value');

		$lesson = $this->getLesson($key);
		$flag = $this->getFlag($lesson, $type);

		if(!$flag) {
			$lesson->flags()->create([
				'user_id' => JWTAuth::getUser()->id,
				'type' => $type,
				'value' => $value,
			]);
		} else {
			$flag->update([
				'value' => $value,
			]);
		}

		return $flag;
	}

	private function getLesson($key)
	{
		return Lesson::isPublished()
			->where('slug', $key)
			->orWhere('id', $key)
			->firstOrFail();
	}

	private function getFlag(Lesson $lesson, $type) {
		return $lesson->flags()
			->where('user_id', JWTAuth::getUser()->id)
			->where('type', $type)
			->first();
	}
}