<?php namespace App\Flags\Updates;

use October\Rain\Support\Facades\Schema;
use October\Rain\Database\Updates\Migration;

/**
 * CreateFlagsTable Migration
 */
class CreateFlagsTable extends Migration
{
    public function up()
    {
        Schema::create('app_flags_flags', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->nullableMorphs('flaggable');
            $table->unsignedBigInteger('user_id')->nullable()->index();

            $table->string('type')->nullable();
            $table->integer('value')->nullable();

            $table->string('text')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('app_flags_flags');
    }
}
