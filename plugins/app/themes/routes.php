<?php

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use WApi\ApiException\Http\Middlewares\ApiExceptionMiddleware;

Route::group([
	'prefix' 		=> 'api/v1',
	'namespace'		=> 'App\Themes\Http\Controllers',
	'middleware' 	=> [ApiExceptionMiddleware::class, 'api'],
], function(Router $router) {
	$router->get('/themes', 'ThemesController@index');
	$router->get('/themes/{slug}', 'ThemesController@show');
});