<?php namespace App\Themes;

use Backend;
use System\Classes\PluginBase;

/**
 * themes Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'themes',
            'description' => 'No description provided yet...',
            'author'      => 'app',
            'icon'        => 'icon-heart'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'App\Themes\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'app.themes.some_permission' => [
                'tab' => 'themes',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'themes' => [
                'label'       => 'Themes',
                'url'         => Backend::url('app/themes/themes'),
                'icon'        => 'icon-heart',
                'permissions' => ['app.themes.*'],
                'order'       => 1,
            ],
        ];
    }
}
