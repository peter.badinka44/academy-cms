<?php namespace App\Themes\Models;

use Model;
use System\Models\File;
use App\Courses\Models\Course;

/**
 * Theme Model
 */
class Theme extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\SoftDelete;
    use \October\Rain\Database\Traits\Purgeable;

    /**
     * @var string table associated with the model
     */
    public $table = 'app_themes';

    /**
     * @var array guarded attributes aren't mass assignable
     */
    protected $guarded = ['*'];

    /**
     * @var array fillable attributes are mass assignable
     */
    protected $fillable = [];

    /**
     * @var array rules for validation
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [
        'is_published' => 'boolean',
    ];

    protected $purge = [
        'courses_repeater'
    ];

    protected $purgeable = [
        'courses_repeater'
    ];

    /**
     * @var array jsonable attribute names that are json encoded and decoded from the database
     */
    protected $jsonable = [];

    /**
     * @var array appends attributes to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array hidden attributes removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array dates attributes that should be mutated to dates
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @var array hasOne and other relations
     */
    public $hasOne = [];
    public $hasMany = [
        'courses' => [
            \App\Courses\Models\Course::class,
            'order' => 'sort_order'
        ]
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'image' => File::class,
    ];
    public $attachMany = [];

    /**
     * Scopes
     */
    public function scopeIsPublished($query)
    {
        return $query->where('is_published', true);
    }

    public function getCoursesRepeaterAttribute()
    {
        return $this->courses->map(function($course) {
            return ['id' => $course->id];
        })->toArray();
    }

    /**
     * Form after save
     */
    public function afterSave()
    {
        if (!$this->getOriginalPurgeValue('courses_repeater')) {
            return;
        }

        $this->courses()->update([
            'theme_id'   => null,
            'sort_order' => null,
        ]);

        collect($this->getOriginalPurgeValue('courses_repeater'))->each(function($courseKey, $index) {
            $course = Course::find($courseKey['id']);
            $course->sort_order = $index;
            $this->courses()->add($course);
        });

    }
}
