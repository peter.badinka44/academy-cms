<?php namespace App\Themes\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateThemesTable Migration
 */
class CreateThemesTable extends Migration
{
    public function up()
    {
        Schema::create('app_themes', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('name')->nullable()->index();
            $table->text('description')->nullable();
            
            $table->text('tags_string')->nullable();
            $table->string('slug')->nullable()->unique();
            
            $table->string('legacy_filter')->nullable()->index();
            $table->string('legacy_theme_courses_map_hash')->nullable()->index();
            
            $table->boolean('is_published')->default(false)->index();
            $table->unsignedInteger('sort_order')->default(0)->index();
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('app_themes');
    }
}