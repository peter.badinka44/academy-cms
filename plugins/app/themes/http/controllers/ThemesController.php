<?php

namespace App\Themes\Http\Controllers;

use App\Themes\Http\Resources\ThemesResource;
use App\Themes\Models\Theme;
use Illuminate\Routing\Controller;

class ThemesController extends Controller
{
	public function index()
	{
		return ThemesResource::collection(
			Theme::isPublished()
				->paginate(get('result_per_page') ?? 100000)
		);
	}

	public function show($slug)
	{
		return new ThemesResource(
			Theme::isPublished()
				->where('slug', $slug)
				->firstOrFail()
		);
	}
}