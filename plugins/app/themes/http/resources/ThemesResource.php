<?php

namespace App\Themes\Http\Resources;

use App\Courses\Http\Resources\CoursesResource;
use Illuminate\Http\Resources\Json\Resource;

class ThemesResource extends Resource
{
	public function toArray($request)
	{
		return [
			'id'			=> $this->id,
			'name'			=> $this->name,
			'description'	=> $this->description,
			'slug'			=> $this->slug,
			'tags'			=> $this->tags,
			'tags_string'	=> $this->tags_string,
			'filter'		=> $this->filter,
			'hash'			=> $this->hash,
			'order'			=> $this->sort_order,
			'image'			=> $this->image,
			'courses'		=> CoursesResource::collection(
				$this->courses()->isPublished()->get()
			),
			'created_at'	=> $this->created_at->toDateTimeString(),
			'updated_at'	=> $this->updated_at->toDateTimeString(),
		];
	}
}