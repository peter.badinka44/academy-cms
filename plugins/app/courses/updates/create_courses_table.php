<?php namespace App\Courses\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateCoursesTable Migration
 */
class CreateCoursesTable extends Migration
{
    public function up()
    {
        Schema::create('app_courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable()->index();
            $table->string('slug')->nullable()->unique();
            $table->text('description')->nullable();
            $table->text('legacy_long_description')->nullable();
            $table->text('tags_string')->nullable();
            $table->text('video_tags_string')->nullable();
            $table->unsignedInteger('sort_order')->nullable()->index();
            $table->boolean('is_published')->default(false)->index();
            $table->unsignedInteger('theme_id')->nullable()->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('app_courses');
    }
}
