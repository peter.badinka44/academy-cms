<?php

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use WApi\ApiException\Http\Middlewares\ApiExceptionMiddleware;

Route::group([
	'prefix'		=> 'api/v1',
	'namespace'		=> 'App\Courses\Http\Controllers',
	'middleware'	=> [ApiExceptionMiddleware::class, 'api'],
], function(Router $router) {  
	$router->get('courses', 'CoursesController@index');
	$router->get('courses/{slug}', 'CoursesController@show');
});