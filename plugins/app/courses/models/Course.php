<?php namespace App\Courses\Models;

use Model;
use System\Models\File;
use App\Lessons\Models\Lesson;

/**
 * Course Model
 */
class Course extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\SoftDelete;
    use \October\Rain\Database\Traits\Purgeable;

    /**
     * @var string table associated with the model
     */
    public $table = 'app_courses';

    /**
     * @var array guarded attributes aren't mass assignable
     */
    protected $guarded = ['*'];

    /**
     * @var array fillable attributes are mass assignable
     */
    protected $fillable = [];

    /**
     * @var array rules for validation
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /*
     * Purgeable attributes
     */
    protected $purgeable = [
        'lessons_repeater',
    ];
    
    protected $purge = [
        'lessons_repeater',
    ];

    /**
     * @var array jsonable attribute names that are json encoded and decoded from the database
     */
    protected $jsonable = [];

    /**
     * @var array appends attributes to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array hidden attributes removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array dates attributes that should be mutated to dates
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @var array hasOne and other relations
     */
    public $hasOne = [];
    public $hasMany = [
        'lessons' => [
            \App\Lessons\Models\Lesson::class,
            'order' => 'sort_order'
        ]
    ];
    public $belongsTo = [
        'theme' => \App\Themes\Models\Theme::class,
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'image' => File::class,
    ];
    public $attachMany = [];

    /**
     * Scopes
     */
    public function scopeIsPublished($query) {
        return $query->where('is_published', true);
    }

    public function getLessonsRepeaterAttribute()
    {
        return $this->lessons->map(function($lesson) {
            return ['id' => $lesson->id];
        })->toArray();
    }

    /*
     * Form after save
     */
    public function afterSave()
    {
        if (!$this->getOriginalPurgeValue('lessons_repeater')) {
            return;
        }
        
        $this->lessons()->update([
            'course_id'  => null,
            'sort_order' => null,
        ]);
        
        collect($this->getOriginalPurgeValue('lessons_repeater'))->each(function($lessonKey, $index) {
            $lesson = Lesson::find($lessonKey['id']);
            $lesson->sort_order = $index;
            $this->lessons()->add($lesson);
        });
    }
}