<?php

namespace App\Courses\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Lessons\Http\Resources\LessonsResource;

class CoursesResource extends Resource
{
	public function toArray($request)
	{
		return [
			'id'				=> $this->id,
			'name'				=> $this->name,
			'slug'				=> $this->slug,
			'tags_string'		=> $this->tags_string,
			'tags'				=> $this->tags,
			'image'				=> $this->image,
			'description'		=> $this->description,
			'long_description'	=> $this->legacy_long_description,
			'lessons_count'		=> $this->lessons_count,
			'created_at'		=> $this->created_at->toDateTimeString(),
			'updated_at'		=> $this->updated_at->toDateTimeString(),
		];
	}
}