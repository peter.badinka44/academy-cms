<?php namespace App\Courses\Http\Controllers;

use App\Courses\Http\Resources\CoursesResource;
use Illuminate\Routing\Controller;
use App\Courses\Models\Course;

class CoursesController extends Controller
{
	/**
	 * index
	 */
	public function index()
	{
		return CoursesResource::collection(
			Course::isPublished()
				->paginate(get('result_per_page') ?? 100000)
		);
	}

	/**
	 * show
	 */
	public function show($slug)
	{
		return new CoursesResource(
			Course::isPublished()
				->where('slug', $slug)
				->firstOrFail()
		);
	}

}
