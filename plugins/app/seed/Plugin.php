<?php namespace App\Seed;

use System\Classes\PluginBase;

/**
 * Seed Plugin Information File
 */
class Plugin extends PluginBase
{
    
    public function pluginDetails()
    {
        return [
            'name'        => 'Seed',
            'description' => 'No description provided yet...',
            'author'      => 'App',
            'icon'        => 'icon-leaf',
        ];
    }
    
    
    public function boot()
    {
    
    }
}
