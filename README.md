# Academy 3.0 BackEnd

## Installation

$ cp .env.example .env

set your `.env` variables

**Install dependencies using composer**

$ composer i -o

$ php artisan october:up

$ composer u -o

**Generate APP KEY**

$ php artisan key:generate --show

**Reset Admin password**

$ php artisan october:passwd